import React from 'react';
import './App.scss';
import AppRouter from './route/index';

function App() {
  return (
    <React.Fragment>
      <AppRouter />
    </React.Fragment>
  );
}

export default App;
