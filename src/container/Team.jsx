import React from "react";
import "../App.css";

class Dashboard extends React.Component {
  constructor(props) {
    super();
    this.state = {
      firstName: "Jasper"
    };
  }
  componentDidMount() {
    let name = this.state.firstName;
    this.props.callBack(name);
  }
  render() {
    let Team = [
      {
        username: "Samppa Nori",
        date: "2012/01/01",
        role: "Member",
        status: "Active"
      },
      {
        username: "Estavan Lykos",
        date: "2012/02/01",
        role: "Staff",
        status: "Banned"
      },
      {
        username: "Chetan Mohamed",
        date: "2012/02/01",
        role: "Admin",
        status: "Inactive"
      },
      {
        username: "Derick Maximinus",
        date: "2012/03/01",
        role: "Member",
        status: "Pending"
      }
    ];
    return (
      <div>
        <div className="card-body">
          <div className="table-responsive">
            <table className="table">
              <thead>
                <tr>
                  <th>{this.props.username}</th>
                  <th>{this.props.date}</th>
                  <th>{this.props.role}</th>
                  <th>{this.props.status}</th>
                </tr>
              </thead>
              <tbody>
                {Team.map((d, i) => {
                  return (
                    <tr>
                      <td>{d.username}</td>
                      <td>{d.date}</td>
                      <td>{d.role}</td>
                      <td>
                        <span className="badge">{d.status}</span>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default Dashboard;
