import React from "react";
import "../App.css";
import Table from "./Table";
import Technology from "./Technology";
class Dashboard extends React.Component {
  constructor(props) {
    super(props);

  }
  render() {
    let tableHeader = {
      username: "Username",
      date: "Date registered",
      role: "Role",
      status: "Status"
    };
    return (
      <div>
        <div className="col-12 mt-4">
          <div className="card">
            <div className="card-header">
              <i className="fa fa-align-justify"></i> Table
            </div>
            <Table {...tableHeader}/>
            <Technology test = {this.state.name}/>
          </div>
        </div>
      </div>
    );
  }
}

export default Dashboard;
