import React from 'react';
import '../../App.css';
import { NavLink } from "react-router-dom";

function Header() {
    return (
        <div className="col-sm-12">
            <div className="row mt-3 align-items-center justify-content-between">
                <div className="logo">
                    React.js
                </div>
                <div className="site-navbar">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <NavLink className="nav-link" to="/dashboard">Dashboard </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" to="/technology">Technologies</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" to="/contact">Contact</NavLink>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    );
}

export default Header;
