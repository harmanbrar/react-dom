import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Footer from "../components/footer/Footer";
import Header from '../components/header/Header';
import Dashboard from '../container/Dashboard';
import Technology from '../container/Technology';
import Contact from '../container/contact';



const AppRouter =()=> {
  return (
    <Router>
      <Header />
      <Switch>
        <Route path="/dashboard" component={Dashboard} />
        <Route path="/technology" component={Technology} />
        <Route path="/contact" component={Contact} />
      </Switch>
      {/* <Footer /> */}
    </Router>
  );
}

export default AppRouter;